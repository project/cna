<?php

namespace Drupal\cna\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class CNAConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cna_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cna.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cna.settings');

    $form = [];
    $form['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select below options'),
    ];
    $form['fieldset']['cna_new'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('New comment'),
      '#default_value' => $config->get('cna_new'),
      '#description' => $this->t('Notify when new comment is posted'),
    ];
    $form['fieldset']['cna_update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update to the comment'),
      '#default_value' => $config->get('cna_update'),
      '#description' => $this->t('Notify when comment is updated by the user'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('cna.settings')
      ->set('cna_new', $values['cna_new'])
      ->set('cna_update', $values['cna_update'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
