## CONTENTS OF THIS FILE

- Introduction
- Recommended modules
- Requirements
- Installation
- Configuration
- Maintainers

## INTRODUCTION

Comment Notify Node Author is the simple yet useful module, sends the email
notification to the author of the node, whenever a new comment is posted or the
existing comment is updated on their content.

## RECOMMENDED MODULES

- [SMTP Authentication Support](https://www.drupal.org/project/smtp) - For
  configuring the mail system.

## REQUIREMENTS

This module requires the comment module which comes along with the drupal core.
Additionally, configure the mail system which is required for sending emails
(Preferably SMTP).

## INSTALLATION

Install the module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

## CONFIGURATION

1. Enable the module from the Modules admin page.

- /admin/modules

2. Navigate to Administration > Configure > System > Comment Notify Node Author
   to configure the module

- /admin/config/system/cna

## MAINTAINERS

- Gowthamraj Krishnasamy - https://www.drupal.org/u/gowthamrajkrishnasamy

Supporting organizations:

- ITT Digital - https://www.drupal.org/itt-digital
